import requests
from pysendpulse.pysendpulse import PySendPulse

def get_info():
    id = 'f8f250e61a87205622fae1e313bde22f'
    secret_id = 'f322faf3d9ce5b94bce442b0afa37ddb'
    TOKEN_STORAGE = 'memcached'

    sp_api = PySendPulse(id, secret_id, TOKEN_STORAGE)

    return sp_api

    # a = sp_api.get_list_of_addressbooks()
    # name_list = []
    # for i in a:
    #     name_list.append((i['name'],i['id']))
    #
    # print(name_list)