import requests


def registration_status(url, name, phone, email):
    requests.post('https://amopro.ru/amo-iama/api/slug/bizon-reg',
                  json={'url': url, 'name': name, 'phone': phone, 'email': email})


def webinar_visited_status(url, name, phone, email):
    requests.post('https://amopro.ru/amo-iama/api/slug/bizon-webinar',
                  json={'url': url, 'name': name, 'phone': phone, 'email': email})
