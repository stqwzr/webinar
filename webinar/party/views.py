from django.shortcuts import render
import json
from django.http import HttpResponse, Http404
from .models import Participant, Course
from django.views.decorators.csrf import csrf_exempt
from background_task import background
from crmApi.api import registration_status
from party.tasks import bizon_get_list
from sendpulseApi.api import get_info
import xmltodict
import logging


@csrf_exempt
def new_party(request):
    try:
        data = json.loads(request.body)
    except Exception:
        raise Http404("Invalid json request")
    new_job = Participant.objects.create(**data)
    new_job.save()
    registration_status('some url', data['name'], data['number'], data['email'])
    file = open('log.txt', 'w')
    file.write('new user added ' + data['name'] + '')
    file.close()
    return HttpResponse(status=201, content_type='application/json')


#TODO Сделать таск для
@background(schedule=30)
def logger():
    print('123')


def index(request):
    logger()
    a = bizon_get_list(repeat=20)
    return render(request, 'index.html')


SP_LIST = [('Upsell', 2245369), ('Продажа основного курса', 2245368),
           ('Купил недельный доступ', 2245367), ('Дожим недельного доступа', 2245362),
           ('Партнерская ссылка', 2245360), ('Видеокурс', 2245358), ('Раздельная оплата', 2245357),
           ('Серия дожима', 2245356), ('Серия знакомства', 2245352), ('Экспресс-курс', 2245337),
           ('Купили основной продукт', 2221855), ('Не купили курс на вебинаре', 2217327),
           ('Купили Upsell', 2217309), ('Купили трипваер', 2217263), ('Подписались на вебинар', 2212294)]


AMO_LIST = {
    'express' : 1,
    'dozhim' : 2,
    'znakomstvo' : 3 ,
    'dozhim-nedel-dostup' : 4,
    'kupil-nedel-dostup' : 5,
    'upsale' : 6,
    'prodazha-osn-kursa' : 7,
    'razdel-oplata' : 8,
    'video-kurs' : 9,
    'partner-oplata' : 10
}

@csrf_exempt
def sendpulse(request, slug):
    # print(slug)
    # a = get_info()
    # logger = logging.getLogger(__name__)
    # logger.info(request.body)
    # logger.info(slug)
    data = request.body

    return HttpResponse(status=200, content_type='application/json')


@csrf_exempt
def paybox(request):
    xpars = xmltodict.parse(request.text)
    data = json.dumps(xpars)
    logger = logging.getLogger(__name__)
    logger.info(data)
