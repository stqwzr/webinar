from django.db import models


# Create your models here.

class Participant(models.Model):
    name = models.CharField(max_length=40)
    email = models.EmailField(max_length=30)
    number = models.CharField(max_length=15)

    STATUS_CHOICES = (
        ('bizon-reg', 'Registered'),
        ('bizon-express', 'Buy express'),
        ('bizon-off', 'Not take part'),
        ('bizon-webinar', 'Take part'),
        ('bizon-dozhim', 'Dozhim'),
    )

    status = models.CharField(max_length=15, choices=STATUS_CHOICES, default='bizon-reg')


class Course(models.Model):
    room_id = models.CharField(max_length=15)
    page_id = models.CharField(max_length=15)
    closestDate = models.DateTimeField()