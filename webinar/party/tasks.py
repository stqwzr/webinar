from background_task import background
from bizonApi.api import auth, subPages, getSubscrivers
from crmApi.api import registration_status, webinar_visited_status


@background(schedule=10)
def bizon_get_list():
    webinar = subPages()
    # print(webinar)
    pageId = webinar['pages'][0]['pageId']
    # print(pageId)
    response = getSubscrivers(pageId)
    people = []
    for i in response['list']:
        # print(i)
        # print(i.get('username'))
        if i.get('visitWebinar')!=None:
            webinar_visited_status('url', i['username'],i['email'], i['phone'])
        registration_status('url', i['username'],i['email'], i['phone'])

# @background(schedule=10)
# def crm_get_list():
#     webinar = subPages()
#     print(webinar)
